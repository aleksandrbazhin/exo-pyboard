from builtins import staticmethod

import time

from OCPacket import OCPacket


class OCPortManager:
    def __init__(self, uart, debug_uart=None):
        self.uart = uart
        self.rcv_buf = b''
        self.debug_uart = debug_uart

    # reads from uart buffer one valid-looking packet
    def _read_uart(self):
        buf = self.uart.read(32)  # some value large enough
        if buf is not None:
            self.rcv_buf += buf
        result_packet = None
        if len(self.rcv_buf) > 0:
            self.rcv_buf, rcv_pck = OCPacket.popleft_from_buffer(self.rcv_buf)
            if rcv_pck:
                result_packet = rcv_pck
        if result_packet is not None:  # empty receive buffer on successful read
            self.rcv_buf = b''
        return result_packet

    def _write_uart(self, data):
        self.uart.write(data)

    def tx_rx(self, tx_packet, retries=5):
        self._write_uart(tx_packet)

        rx_packet = None
        for _ in range(retries):
            time.sleep_ms(1)
            rx_packet = self._read_uart()
            if rx_packet is not None:
                break
            else:
                self._write_uart(tx_packet)
                if self.debug_uart is not None:
                    self.debug_uart.write('LOOSING PACKAGES! (' + str(tx_packet) + ')\r\n')

        if self.debug_uart is not None:
            self.debug_uart.write('TX: ' + str(tx_packet) + '\r\n')
            self.debug_uart.write('RX: ' + str(rx_packet) + '\r\n')

        return rx_packet


class OCServo:
    position_max = 4095

    def __init__(self, port_manager, servo_id):
        self.id = servo_id
        self.port = port_manager
        self.errors = []

    def _request_reply(self, instruction, parameters=None):
        send_packet = OCPacket(self.id, instruction, parameters)
        send_packet_raw = send_packet.form_packet()
        receive_pck_raw = self.port.tx_rx(send_packet_raw)
        receive_packet = OCPacket.parse_packet(receive_pck_raw)
        if receive_packet is not None and receive_packet['id'] == self.id:
            self.errors = receive_packet['errors']
            return receive_packet
        else:
            return None

    @staticmethod
    def form_read_parameters(start_register_name, params_lengths):
        address = OCPacket.memory_map[start_register_name]
        length_as_bytes = OCPacket.as_bytes(sum(params_lengths))
        return address + length_as_bytes

    @staticmethod
    def form_write_parameters(start_register_name, params_bytes):
        address = OCPacket.memory_map[start_register_name]
        return address + params_bytes

    @staticmethod
    def degree_to_position(degree):
        return int(degree * OCServo.position_max / 360.0).to_bytes(2, 'little')

    @staticmethod
    def position_to_degree(position):
        return float(position / OCServo.position_max * 360.0)

    def ping(self):
        response = self._request_reply('ping')
        if response is not None:
            return True
        else:
            return False

    # @param_name: name of register from which start reading (see memory_map in OCPacket and datasheet)
    # @params_lengths: length of parameters in bytes
    def get_from_servo(self, param_name, params_lengths):
        params_bytes = OCServo.form_read_parameters(param_name, params_lengths)
        response_packet = self._request_reply('read', params_bytes)
        if response_packet is not None:
            params = OCPacket.parse_params(response_packet['params'], *params_lengths)
            return params or None
        return None

    # returns voltage in floats or None
    def get_voltage(self):
        response_params = self.get_from_servo('Current voltage', (1,))
        if response_params is not None:
            return response_params[0] / 10.0
        return None

    # returns current position
    def get_position(self):
        response_params = self.get_from_servo('Current position(L)', (2,))
        if response_params is not None:
            return OCServo.position_to_degree(response_params[0])
        return None

    # gets servo goal position (position 0 - 360.0)
    def get_goal_position(self):
        response_params = self.get_from_servo('Goal position(L)', (2,))
        if response_params is not None:
            return OCServo.position_to_degree(response_params[0])
        return None

    # moves servo to position (position 0 - 360.0)
    def set_goal_position(self, position):
        params_bytes = OCServo.form_write_parameters('Goal position(L)',
                                                     OCServo.degree_to_position(position))
        response_packet = self._request_reply('write', params_bytes)
        if response_packet is not None:
            return True
        else:
            return None





