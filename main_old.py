from pyb import LED, Pin, ADC, USB_VCP, UART
from OCServo import OCServo, OCPortManager
from ExoControl import ExoPortHandler, ExoController, JointDevice
import os

adc1 = ADC(Pin('Y11'))
adc2 = ADC(Pin('Y12'))
# led = LED(3)  # 1=red, 2=green, 3=yellow, 4=blue
# led_scs = LED(2)

usb = USB_VCP()

# TODO: убрать костыль (подтянуть на HIGH на схеме)
ocservo_enable_pin1 = pyb.Pin(pyb.Pin.board.X19, pyb.Pin.OUT_PP)
ocservo_enable_pin1.value(True)
ocservo_enable_pin2 = pyb.Pin(pyb.Pin.board.X20, pyb.Pin.OUT_PP)
ocservo_enable_pin2.value(True)

servo_uart = UART(2)
servo_uart.init(baudrate=1000000, timeout=0)
servo_port_manager = OCPortManager(servo_uart, debug_uart=usb)
elbow_servo = OCServo(servo_port_manager, servo_id=1)

hc06_uart = UART(4)
hc06_uart.init(baudrate=9600, bits=8, parity=None, stop=1)
exo_command_port = ExoPortHandler(hc06_uart, debug_uart=usb)

# if not os.path.isdir('settings'):
try:
    os.mkdir('settings')
except:
    pass

elbow_joint_device = JointDevice(elbow_servo, adc1, adc2, settings_file='settings/elbow1.set', debug_uart=usb)
exo_skeleton = ExoController(exo_command_port, (elbow_joint_device, ), debug_uart=usb)
exo_skeleton.start_command_loop()

