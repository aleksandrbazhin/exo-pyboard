from builtins import staticmethod


class OCPacket:
    pck_header = b'\xff\xff'

    def __init__(self, device_id, instruction_name, params=b''):
        self.id = OCPacket.as_bytes(device_id)
        self.data_len = OCPacket.as_bytes(len(params) + 2)

        self.instruction = self.instruction_table[instruction_name]
        self.data = params

    def form_packet(self):
        pck = self.id + self.data_len + self.instruction + self.data
        return self.pck_header + pck + OCPacket.compute_crc(pck)

    # number should be int and length 1 (< 256)
    @staticmethod
    def as_bytes(number, length=1):
        return number.to_bytes(length, 'little')

    @staticmethod
    def compute_crc(pck_data):
        return OCPacket.as_bytes(~(sum(pck_data) & 0xff) & 0xff)

    # read bytes from buffer until packet start encountered
    # returns packet and remainder of buffer
    @staticmethod
    def popleft_from_buffer(buffer):
        i = 0
        packet = b''
        buffer_read_index = 0
        while i < len(buffer) - 2:
            if buffer[i: i+2] == OCPacket.pck_header:
                data_len = buffer[i + 3]
                if len(buffer) > i + data_len:        # full packet received
                    buffer_read_index = i + 4 + data_len
                    packet = buffer[i: buffer_read_index]
                else:
                    buffer_read_index = i
                break
            i += 1
        return buffer[buffer_read_index:], packet

    @staticmethod
    def parse_packet(pck):
        if pck is None or not OCPacket.compute_crc(pck[2: -1]) == pck[-1:]:
            return None
        return {'id': pck[2],
                'data_len': pck[3],
                'errors': OCPacket.parse_error(pck[4]),
                'params': pck[5:-1]
                }

    @staticmethod
    def parse_error(err_byte):
        if not err_byte:
            return None
        errors = []
        for key, err in OCPacket.error_table.items():
            if err_byte & (0b1 << key):  # checking every bit
                errors.append(err)
        return errors

    # lengths - sizes of parameters in bytes (some are 2 with low and high some are 1)
    @staticmethod
    def parse_params(params, *lengths):
        if not params or len(lengths) == 0:
            return None
        values = []
        i = 0
        for value_len in lengths:
            if i + value_len > len(params):
                break
            if value_len == 1:
                values.append(params[i])
            elif value_len == 2:
                value = int.from_bytes(params[i:i+2], 'little', False)
                values.append(value)
            else:                   # garbage in params (not 1 and 2 in lenghts)
                return None
            i += value_len
        return values

    error_table = {
        0: 'voltage',
        1: 'angle',
        2: 'temperature',
        3: 'current',
        5: 'overload'
    }

    instruction_table = {
        'ping': b'\x01',
        'read': b'\x02',
        'write': b'\x03',
        'reg_write': b'\x04',
        'action': b'\x05',
        'sync_write': b'\x83',
        'bulk_write_data': b'\x09',
        'reset': b'\x06'
    }

    memory_map = {
        'Firmware version(L)': b'\x03',
        'Firmware version(H)': b'\x04',
        'Servo ID': b'\x05',
        'BaudRate': b'\x06',
        'Response delay': b'\x07',
        'Response level': b'\x08',
        'Min angel limit(L)': b'\x09',
        'Min angel limit(H)': b'\x0A',
        'Max angel limit(L)': b'\x0B',
        'Max angel limit(H)': b'\x0C',
        'Max Temperature limit': b'\x0D',
        'Max input voltage': b'\x0E',
        'Min input voltage': b'\x0F',
        'Max torque(L)': b'\x10',
        'Max torque(H)': b'\x11',
        'Motor Driven PWM phase': b'\x12',
        'Uninstall condition': b'\x13',
        'LED Alarm condition': b'\x14',
        'PID: P Gain': b'\x15',
        'PID: D Gain': b'\x16',
        'PID: I Gain': b'\x17',
        'Start power(L)': b'\x18',
        'Start power(H)': b'\x19',
        'CW dead band width': b'\x1A',
        'CCW dead band width': b'\x1B',
        'Max integral limit(L)': b'\x1C',
        'Max integral limit(H)': b'\x1D',
        'Output shaft neutral point correction(L)': b'\x21',
        'Output shaft neutral point correction(H)': b'\x22',
        'Running mode': b'\x23',
        'Protect current': b'\x24',
        'Torque switch': b'\x28',
        'Goal position(L)': b'\x2A',
        'Goal position(H)': b'\x2B',
        'Operation time(L)': b'\x2C',
        'Operation time(H)': b'\x2D',
        'Operation speed(L)': b'\x2E',
        'Operation speed(H)': b'\x2F',
        'Lock sign': b'\x30',
        'Relative movement sign': b'\x33',
        'Current position(L)': b'\x38',
        'Current position(H)': b'\x39',
        'Current speed(L)': b'\x3A',
        'Current speed(H)': b'\x3B',
        'Current lead(L)': b'\x3C',
        'Current lead(H)': b'\x3D',
        'Current voltage': b'\x3E',
        'Current temperature': b'\x3F',
        'REG WRITE sign': b'\x40',
        'Servo operating signs': b'\x42',
        'The current goal location(L)': b'\x43',
        'The current goal location(H)': b'\x44',
        'Current current(L)': b'\x45',
        'Current current(H)': b'\x46'
    }