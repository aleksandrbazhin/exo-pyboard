import ubinascii
import os

class ExoPacket:
    packet_start = b'\x3a'
    packet_end = b'\r\n'

    def __init__(self, device_id, function_id, parameters=b''):
        self.device_id = device_id
        self.function_id = function_id
        self.parameters = parameters
        # raw_bytes = self.form_packet()
        # self.lrc = self.compute_lrc(raw_bytes)

    @staticmethod
    def as_ascii(value, length=1):
        if isinstance(value, int):
            value = value.to_bytes(length, 'big')
        return ubinascii.hexlify(value)

    @staticmethod
    def from_ascii(value):
        return ubinascii.unhexlify(value)

    # ascii without lrc and packet start / end
    @staticmethod
    def parse_ascii(raw_ascii):
        device_id = ExoPacket.from_ascii(raw_ascii[0:2])
        function_id = ExoPacket.from_ascii(raw_ascii[2:4])
        parameters = ExoPacket.from_ascii(raw_ascii[4:-2])
        return device_id, function_id, parameters

    @classmethod
    def from_raw_bytes(cls, pck):
        if pck is None \
                or len(pck) % 2 != 0 \
                or not ExoPacket.compute_lrc(pck[: -2]) == pck[-2:]:
            return None
        device_id, function, parameters = ExoPacket.parse_ascii(pck)
        packet = cls(device_id, function, parameters)
        return packet

    def form_packet(self):
        pck = b''
        pck += ExoPacket.as_ascii(self.device_id)
        pck += ExoPacket.as_ascii(self.function_id)
        pck += self.parameters
        pck += ExoPacket.compute_lrc(pck)
        return ExoPacket.packet_start + pck + ExoPacket.packet_end

    @staticmethod
    def compute_lrc(pck):
        pck_bytes = ExoPacket.from_ascii(pck)
        lrc = (- sum(pck_bytes)) & 0xff
        return ExoPacket.as_ascii(lrc)


class ExoPortHandler:
    min_rcv_buf = 5
    max_rcv_buf = 32

    def __init__(self, uart, debug_uart = None):
        self.uart = uart
        self.rcv_buf = b''
        self.debug_uart = debug_uart

    def check_new_input(self):

        # self.uart.write("checkin\r\n")
        incoming = self.uart.any()
        if incoming > 0:
            self.rcv_buf += self.uart.read(incoming)
        else:
            return None
        if len(self.rcv_buf) < ExoPortHandler.min_rcv_buf:
            return None
        i = 0
        raw_packet_data = None
        packet_start_index = 0
        in_packet = False
        buf_read_index = 0
        while i < len(self.rcv_buf) - 1:
            if self.rcv_buf[i: i + 1] == ExoPacket.packet_start:
                packet_start_index = i + 1
                in_packet = True
            if in_packet:
                if self.rcv_buf[i: i + 2] == ExoPacket.packet_end:
                    raw_packet_data = self.rcv_buf[packet_start_index: i]
                    buf_read_index = i + 1
                    break
            else:
                buf_read_index = i
            i += 1
        if self.debug_uart is not None:
            self.debug_uart.write('received: ' + str(self.rcv_buf) + '\r\n')
            self.debug_uart.write('PACKET FOUND: ' + str(raw_packet_data) + '\r\n')

        self.rcv_buf = self.rcv_buf[buf_read_index:]
        return raw_packet_data

    def send_packet(self, packet):
        ascii_packet = packet.form_packet()
        if self.debug_uart is not None:
            self.debug_uart.write('WRITING PACKET: ' + str(ascii_packet) + '\r\n')
        self.uart.write(ascii_packet)


class ExoController:
    # take servo objects as list
    def __init__(self, command_port, devices, debug_uart=None):
        self.command_port = command_port
        self.debug_uart = debug_uart
        self.devices = {
            b'\x00': GeneralDevice(),
            b'\x01': devices[0],
        }

    def start_command_loop(self):
        while True:
            self.automated_control()
            command_bytes = self.command_port.check_new_input()

            # self.command_port.write('iteration\r\n')

            if command_bytes is not None:
                packet = ExoPacket.from_raw_bytes(command_bytes)
                if packet is not None:  # valid packet + lrc

                    self.debug_uart.write('packet: id=' + str(packet.device_id) +
                                          ', function=' + str(packet.function_id) +
                                          ', data=' + str(packet.parameters) + '\r\n')

                    return_packet_params = self.run_packet(packet)
                    if return_packet_params is not None:
                        return_packet = ExoPacket(packet.device_id,
                                                  packet.function_id,
                                                  return_packet_params)
                        self.command_port.send_packet(return_packet)
                else:

                    self.debug_uart.write('INVALID PACKET \r\n')

    def automated_control(self):
        # check if position > max or < min - apply force
        # check voltage
        # check temp
        # get mio
        # apply force to servo ~ mio
        for key, device in self.devices.items():
            device.exec_auto_control()

    def run_packet(self, packet):
        if packet.device_id not in self.devices:
            return None
        device = self.devices[packet.device_id]
        if packet.function_id not in device.functions:
            return None
        return device.run_command(
            packet.function_id,
            packet.parameters,
        )


class GeneralDevice:
    functions = {
        b'\x01': 'PING',
        b'\x02': 'READ',
        b'\x03': 'WRITE',
    }

    default_memory = {
        b'\x00': 0x0001,  # version
        b'\x01': 0x0000,  # battery charge
        b'\x02': 0x0000,  # error
    }

    def __init__(self):
        self.functions = GeneralDevice.functions
        self.memory = GeneralDevice.default_memory.copy()

    # to interface
    def run_command(self, function_id, params=None):
        if self.functions[function_id] == 'PING':
            return b''
        elif self.functions[function_id] == 'READ':
            return b''
        elif self.functions[function_id] == 'WRITE':
            return b''

    # to interface
    def exec_auto_control(self):
        pass


class JointDevice:
    reduction_ratio = 1.0
    adc_max = 4096
    functions = {
        b'\x01': 'PING',
        b'\x02': 'READ',
        b'\x03': 'WRITE',
    }
    default_settings_raw = b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'

    def __init__(self, servo, adc1=None, adc2=None, settings_file=None, debug_uart=None):
        self.servo = servo
        self.adc1 = adc1
        self.adc2 = adc2
        self.settings_file = settings_file
        self.debug_uart = debug_uart
        self.error = 0b00000000
        self.in_auto_loop = 0
        self.settings = {
            'reduction ratio': JointDevice.reduction_ratio,
        }

        # self.debug_uart.write('\r\n' + str(settings_file) + '\r\n\r\n')
        # if settings_file is not None:
        self.read_settings_from_fs()

        self.memory = {
            0x00: {'READ': self.make_read_setting('reduction ratio', JointDevice.convert_reduction_ratio_to_bytes)},
            0x01: {'READ': self.make_read_setting('mio1 min'),
                   'WRITE': self.make_write_setting('mio1 min', JointDevice.convert_mio_limit_write)},  # mio1 min
            0x02: {'READ': self.make_read_setting('mio1 max'),
                   'WRITE': self.make_write_setting('mio1 max', JointDevice.convert_mio_limit_write)},  # mio1 max
            0x03: {'READ': self.make_read_setting('mio2 min'),
                   'WRITE': self.make_write_setting('mio2 min', JointDevice.convert_mio_limit_write)},  # mio2 min
            0x04: {'READ': self.make_read_setting('mio2 max'),
                   'WRITE': self.make_write_setting('mio2 max', JointDevice.convert_mio_limit_write)},  # mio2 max
            0x05: {'READ': self.make_read_setting('joint angle min'),
                   'WRITE': self.make_write_setting('joint angle min')},  # joint angle min
            0x06: {'READ': self.make_read_setting('joint angle max'),
                   'WRITE': self.make_write_setting('joint angle max')},  # joint angle max
            0x07: {'READ': self.make_read_mio(self.adc1)},  # mio1 current value
            0x08: {'READ': self.make_read_mio(self.adc2)},  # mio2 current value
            0x09: {'READ': self.make_read_joint_angle()},  # joint current angle
            0x0a: {},  # joint current torque
            0x0b: {'READ': self.make_read_voltage()},  # servo current voltage
            0x0c: {},  # torque on
            0x0d: {'READ': self.make_read_joint_target_angle(),
                   'WRITE': self.make_write_joint_target_angle()},  # joint target position
            0x0e: {},  # joint target speed
            0x0f: {'READ': self.make_read_auto_control(),
                   'WRITE': self.make_write_auto_control()},  # autocontrol enabled
            0x10: {},  # error
        }

    def exec_auto_control(self):
        if self.in_auto_loop == 1:
            self.debug_uart.write('i am controlling \r\n')
            pass

    def read_settings_from_fs(self):
        try:
            with open(self.settings_file, 'rb') as f:
                fs_settings_raw = f.read()
            exists = True
        except:
            with open(self.settings_file, 'wb') as f:
                fs_settings_raw = JointDevice.default_settings_raw
                f.write(JointDevice.default_settings_raw)
            exists = False
        self.debug_uart.write('settings file exists: ' + str(exists))
        fs_settings = {
            'mio1 min': fs_settings_raw[0:2],
            'mio1 max': fs_settings_raw[2:4],
            'mio2 min': fs_settings_raw[4:6],
            'mio2 max': fs_settings_raw[6:8],
            'joint angle min': fs_settings_raw[8:10],
            'joint angle max': fs_settings_raw[10:12],
        }
        self.settings.update(fs_settings)
        return exists

    def write_settings_to_fs(self):
        raw_settings_data = b''
        raw_settings_data += self.settings['mio1 min']
        raw_settings_data += self.settings['mio1 max']
        raw_settings_data += self.settings['mio2 min']
        raw_settings_data += self.settings['mio2 max']
        raw_settings_data += self.settings['joint angle min']
        raw_settings_data += self.settings['joint angle max']
        with open(self.settings_file, "wb") as f:
            f.write(raw_settings_data)

    @staticmethod
    def convert_mio_limit_write(limit_bytes):
        limit = int.from_bytes(limit_bytes, 'big')
        if limit < 0 or limit > JointDevice.adc_max:
            limit_bytes = None
        return limit_bytes

    @staticmethod
    def convert_reduction_ratio_to_bytes(ratio):
        ratio = int(1000 * ratio)
        return ratio

    def make_read_setting(self, setting_name, convert_function=None):
        def read_setting():
            if setting_name in self.settings:
                setting = self.settings[setting_name]
                if convert_function is not None:
                    setting = convert_function(setting)
                return setting
            else:
                return None
        return read_setting

    def make_write_setting(self, setting_name, convert_function=None):
        def write_setting(value):
            if setting_name in self.settings:
                result = 0x00
                if convert_function is not None:
                    value = convert_function(value)
                if value is not None:
                    self.settings[setting_name] = value
                    result = 0xff
                    self.write_settings_to_fs()
                return result
            else:
                return None
        return write_setting

    def make_read_mio(self, adc):
        def read_mio():
            if adc is not None:
                return adc.read()
            else:
                return None
        return read_mio

    def make_read_joint_angle(self):
        def read_joint_angle():
            angle = self.servo.get_position()
            if angle is not None:
                angle /= self.settings['reduction ratio']
                angle = int(angle * 10)
            return angle
        return read_joint_angle

    def make_write_joint_target_angle(self):
        def set_joint_target_angle(angle_bytes):
            result = 0x00
            angle = int.from_bytes(angle_bytes, 'big')
            if isinstance(angle, int):
                angle = float(angle) / 10.0 / self.settings['reduction ratio']
                if self.servo.set_goal_position(angle):
                    result = 0xff
            return result
        return set_joint_target_angle

    def make_read_joint_target_angle(self):
        def read_target_joint_angle():
            angle = self.servo.get_goal_position()
            if angle is not None:
                angle /= self.settings['reduction ratio']
                angle = int(angle * 10)
            return angle
        return read_target_joint_angle

    def make_read_voltage(self):
        def read_voltage():
            voltage = self.servo.get_voltage()
            if voltage is not None:
                voltage = int(voltage * 100)
            return voltage
        return read_voltage

    def test_auto_settings(self):
        if self.settings['mio1 min'] < self.settings['mio1 max'] and \
                self.settings['joint angle min'] < self.settings['joint angle max']:
            return True
        else:
            return False

    def make_write_auto_control(self):
        def write_auto_control(value_bytes):
            result = 0x00
            in_auto_loop = int.from_bytes(value_bytes, 'big')
            if in_auto_loop == 0 or (in_auto_loop == 1 and self.test_auto_settings()):
                self.in_auto_loop = in_auto_loop
                result = 0xff
            return result
        return write_auto_control

    def make_read_auto_control(self):
        def read_auto_control():
            return self.in_auto_loop
        return read_auto_control

    def run_command(self, function_id, params=None):
        response_bytes = b''
        if self.functions[function_id] == 'PING':
            pass
        elif self.functions[function_id] == 'READ':
            start_address = params[0]
            read_length = params[1]
            register_count = 0
            response_bytes = ExoPacket.as_ascii(start_address)
            while register_count < read_length:
                register = start_address + register_count
                if register not in self.memory:
                    break
                if 'READ' in self.memory[register]:
                    self.debug_uart.write(
                        'READING FROM REGiSTER: ' + str(register) + '\r\n')
                    read_result = self.memory[register]['READ']()
                    if read_result is not None:
                        response_bytes += ExoPacket.as_ascii(read_result, 2)
                register_count += 1

        elif self.functions[function_id] == 'WRITE':
            start_address = params[0]
            write_length = params[1]
            memory_values = params[2:]
            register_count = 0  # counting memory addresses
            value_count = 0     # counting passed values (every register needs 2)
            response_bytes = ExoPacket.as_ascii(start_address)
            while value_count < len(memory_values):  # writing every value to register
                if register_count > write_length:  # too much values
                    break
                register = start_address + register_count
                if register not in self.memory:
                    break
                if 'WRITE' in self.memory[register]:
                    self.debug_uart.write('WRITING TO REGISTER: ' + str(register)
                                          + 'VALUES ' + str(memory_values[value_count: value_count + 2]) + '\r\n')
                    result = self.memory[register]['WRITE'](memory_values[value_count: value_count + 2])
                    response_bytes += ExoPacket.as_ascii(result)
                register_count += 1
                value_count = register_count * 2

        return response_bytes





