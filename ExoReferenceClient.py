import binascii
import serial
import time


def compute_lrc(pck):
    pck_bin = binascii.unhexlify(pck)
    lrc = (- sum(pck_bin)) & 0xff
    return binascii.hexlify(lrc.to_bytes(1, 'little'))


def form_pck(pck):
    return b':' + pck + compute_lrc(pck) + b'\r\n'


if __name__ == '__main__':
    ser = serial.Serial(port='COM3', baudrate=9600)
    snd_pck = form_pck(b'0001')  # ping
    while True:
        ser.write(snd_pck)
        ser.flush()
        print('send', snd_pck)
        out = b''
        while ser.inWaiting() > 0:
            out += ser.read(1)

        if len(out) > 0:
            print('receiving: ', out)
            ser.write(snd_pck)
        time.sleep(1)