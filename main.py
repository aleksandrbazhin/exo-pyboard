from pyb import LED, Pin, ADC, USB_VCP, UART
from OCServo import OCServo, OCPortManager
from math import copysign
from time import sleep_ms

adc1 = ADC(Pin('Y11'))
adc2 = ADC(Pin('Y12'))

usb = USB_VCP()

ocservo_enable_pin1 = pyb.Pin(pyb.Pin.board.X19, pyb.Pin.OUT_PP)
ocservo_enable_pin1.value(True)
ocservo_enable_pin2 = pyb.Pin(pyb.Pin.board.X20, pyb.Pin.OUT_PP)
ocservo_enable_pin2.value(True)

servo_uart = UART(2)
servo_uart.init(baudrate=1000000, timeout=0)
servo_port_manager = OCPortManager(servo_uart)
elbow_servo = OCServo(servo_port_manager, servo_id=1)

# управление серво
# задаем разницу между датчиками, с которой начинать движение (порог срабатывания)
MYO_THRESHOLD = 400
# шаг по времени в миллисекундах
TIME_STEP_MS = 20

# коэффициент перевода разницы мио в шаг по углу серво за единицу времени
MYO_TO_ANGLE_STEP_COEFFICIENT = (360.0 * TIME_STEP_MS / 1000.0) / 2000.0
# разница между мио может быть до 4096 (а не 1024 как в ардуино), пусть реально до 2000,
# а угол серво от 0 до 360. При макс скорости поворота 360 градусов / сек (или 360 / 1000 мсек)
# получим (360 / 1000 * TIME_STEP_MS) / 2000
# если поиграться с коэффициентами будет по-разному работать
while True:
    myo1 = adc1.read()
    myo2 = adc2.read()

    # разница между показаниями миодатчиков
    myo_difference = abs(myo1 - myo2)
    usb.write("mio1 {}, mio2 {}, diff {}\r\n".format(myo1, myo2, myo_difference))
    if myo_difference > MYO_THRESHOLD:
        # насколько разница превысила threshold
        myo_control_value = myo_difference - MYO_THRESHOLD
        # копируем знак разницы, чтобы узнать направление движения
        myo_control_value = copysign(myo_control_value, myo1 - myo2)
        # узнаем текущий угол локтевого сочленения
        current_angle = elbow_servo.get_position()
        # если серво подключено
        if current_angle is not None:
            # задаем угол, в который надо перейти за шаг времени
            target_angle = current_angle + myo_control_value * MYO_TO_ANGLE_STEP_COEFFICIENT
            # отправляем команду на серво
            elbow_servo.set_goal_position(target_angle)
    sleep_ms(TIME_STEP_MS)
